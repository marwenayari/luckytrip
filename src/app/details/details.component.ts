import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { destination, DestinationService } from '../services/destination.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {

  destinationId: number = 0;
  destination: destination | undefined = undefined;
  searchText: string = "";
  error: number | undefined = undefined;

  constructor(
    private destinationService: DestinationService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.destinationId = params['id'];
      this.getDestination()
    });
  }

  getDestination(){
    this.destinationService.getDestination(this.destinationId)
      .subscribe((data: any) => {
        this.destination = data.destination;
      }, error => {
        this.error = error.status;
      });
  }

  search() {
    this.router.navigate(['/search'], {queryParams: {query: this.searchText}})
  }
}
