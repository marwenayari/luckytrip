import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { destination, DestinationService } from '../services/destination.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  destinations: Array<destination> = [];
  searchText: string = "";
  loaded: boolean = false;
  error: number | undefined = undefined;

  constructor(
    private destinationService: DestinationService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      if (params['query']) {
        this.searchText = params['query'];
        this.search();
      } else {
        this.getDestinations("", "");
        this.searchText = "";
      }
    });
  }

  getDestinations(type: string, value: string){
    this.destinationService.getDestinations(type, value)
      .subscribe((data: any) => {
        this.error = undefined;
        data.destinations.length == 0 ? this.error = 404 : this.destinations = data.destinations;
        this.loaded = true;
      }, error => {
        this.error = error.status;
      });
  }

  go() {
    this.router.navigate(['/search'], {queryParams: {query: this.searchText}})
  }

  search() {
    let dataByCountry: destination[] = [];

    this.destinationService.getDestinations("country", this.searchText).subscribe((data: any) => {
      dataByCountry = data.destinations;

      this.destinationService.getDestinations("city", this.searchText).subscribe((data: any) => {
        this.destinations = dataByCountry.concat(data.destinations);
        this.loaded = true;
        this.destinations.length == 0 ? this.error = 404 : this.error = undefined;;
      });
    });
  }
}
