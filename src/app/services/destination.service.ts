import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { environment } from 'src/environments/environment';

export interface destination {
  id: number;
  city: string;
  airport_name?: string;
  country_name?: string;
  country_code?: string;
  top_fives_new_flag?: number;
  top_fives_updated_flag?: number;
  image_url?: string;
  thumbnail?: {
    image_url?: string;
  };
  description?: {
    text: string;
  };
}

@Injectable({
  providedIn: 'root',
})
export class DestinationService {
  constructor(private http: HttpClient) {}

  getDestinations(type: string, value: string) {
    const url = environment.apiURL + `/destinations?search_type=${type}&search_value=${value}`;
    return this.http.get<destination[]>(url);
  }

  getDestination(id: number) {
    return this.http.get<destination>(environment.apiURL + `/destination?id=${id}`);
  }
}
